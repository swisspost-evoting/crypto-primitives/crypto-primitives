/*
 * Copyright 2024 Swiss Post Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * The classes in this package, ch.post.it.evoting.cryptoprimitives.internal, are intended for internal use within the crypto-primitives only. They
 * contain implementations and details that are essential for the internal workings of the crypto-primitives.
 * <p>
 * For external use, these internal classes should not be accessed directly. Instead, factories are provided in the corresponding packages. These
 * factories provide a public interface for creating and manipulating the crypto-primitives. For external usage, see for example factories such as
 * {@link ch.post.it.evoting.cryptoprimitives.math.RandomFactory}.
 */
package ch.post.it.evoting.cryptoprimitives.internal;